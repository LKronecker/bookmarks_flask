from flask import render_template, redirect, url_for, flash

from thermos import app, db
from thermos.forms import BookmarkForm
from thermos.models import User, Bookmark

#Fake login
def logged_in_user():
    return User.query.filter_by(username='LKroon').first()

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', new_bookmarks=Bookmark.newest(5))

    # Example of passing data to the view through 'render_template'
    # return render_template('index.html', title='Title passed from view function the template',
    #                        user=User(firstname='Leo', lastname='Var'))


@app.route('/add', methods=['GET', 'POST'])
def add():
    ## Using WTF
    form = BookmarkForm()
    if form.validate_on_submit():
        url = form.url.data
        description = form.description.data
        bm = Bookmark(user=logged_in_user(), url=url, description=description)
        db.session.add(bm)
        db.session.commit()
        flash("Stored '{}'".format(description))
        return redirect(url_for('index'))
    print('Form  =======')
    print(form)
    return render_template('add.html', form=form)

    ## Without using WTF:
    # if request.method == 'POST':
    #     url = request.form['url']
    #     store_bookmark(url)
    #     # app.logger.debug('stored url: ' + url)
    #     flash("Stored bookmark '{}'".format(url))
    #     return redirect(url_for('index'))

    # if not post just render the page add.html
    # return render_template('add.html')

@app.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def page_not_found(e):
    return render_template('500.html'), 500


class User:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname

    def initials(self, title):
        return '{}. {}. {}.'.format(title, self.firstname[0], self.lastname[0])


if __name__ == "__main__":
    app.run(debug=True)