import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = '\xb9\x18\x9b\xfc\x06\xc3cW\x14H(\xc5\xe0-_\x96\x9a\xb4\xd48\x1f\xbfQ\x0b'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'thermos.db')
app.config['DEBUG'] = True
db = SQLAlchemy(app)

import thermos.models
import thermos.views