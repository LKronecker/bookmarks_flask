#! /usr/bin/env python

# from thermos.thermos import app, db
from thermos import app, db
from flask_script import Manager, prompt_bool
from thermos.models import User

manager = Manager(app)

@manager.command
def initdb():
    db.create_all()
    db.session.add(User(username='LKroon', email='somerandomemail@email.com'))
    db.session.add(User(username='PythonMaster', email='somerandomemail222@email.com'))
    db.session.commit()
    print('Initialized the databse')


@manager.command
def dropdb():
    if prompt_bool("Are you sure you want to loose all your data?"):
        db.drop_all()
        print("Dropped the databse")


if __name__ == '__main__':
    manager.run()
